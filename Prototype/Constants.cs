﻿using System;
using System.IO;

namespace Prototype
{
    public static class Constants
    {
        private static readonly string UserFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        private static readonly string TempFolderPath = Path.Combine(UserFolder, "Temp");
        private static readonly string FolderName = "target";
        private static readonly string Split = "split";
        private static readonly string Restored = "restored";

        public static readonly int FolderCount = 5;
        public static readonly int FileSize = 128 * 1024;

        public static readonly string TargetFolderPath = Path.Combine(TempFolderPath, FolderName);
        public static readonly string SplitFolderPath = Path.Combine(TempFolderPath, Split);
        public static readonly string MergeFolderPath = Path.Combine(TempFolderPath, Restored);
        
        static Constants(){

            if (!Directory.Exists(TempFolderPath))
                Directory.CreateDirectory(TempFolderPath);

            if (!Directory.Exists(TargetFolderPath))
                Directory.CreateDirectory(TargetFolderPath);

            if (!Directory.Exists(SplitFolderPath))
                Directory.CreateDirectory(SplitFolderPath);

            for (var i = 0; i < FolderCount ; i++)
            {
                var path = Path.Combine(SplitFolderPath, i.ToString());
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }

            if (!Directory.Exists(MergeFolderPath))
                Directory.CreateDirectory(MergeFolderPath);
        }
    }
}
