﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace Prototype
{
    public struct Metadata
    {
        public string FullName;
        public long Length;
        public DateTime CreationTime;
        public string Owner;
        public byte[] Bytes;
        public byte[] HashValue;
    
        public Metadata(string fullName, long length, DateTime creationTime, string owner, byte[] bytes, byte[] hash) : this()
        {
            FullName = fullName;
            Length = length;
            CreationTime = creationTime;
            Owner = owner;
            Bytes = bytes;
            HashValue = hash;
        }
    }

    public static class FileHashDictionary
    {
        private static readonly IDictionary<string, Metadata> Dictionary = new Dictionary<string, Metadata>();

        static FileHashDictionary()
        {
            CalculateFileHash();
        }

        private static void CalculateFileHash()
        {
            // fixme : to utilise file's metadata instead of file itself
            var files = System.IO.Directory.GetFiles(Constants.TargetFolderPath);
            foreach (var file in files)
            {
                var fileInfo = new FileInfo(file);
                var fullName = fileInfo.FullName;
                var length = fileInfo.Length;
                var creationTime = fileInfo.CreationTimeUtc;
                var owner = File.GetAccessControl(file).GetOwner(typeof(System.Security.Principal.NTAccount)).ToString();

                var concatnatedString = string.Concat(fullName, length, creationTime, owner);
                var bytes = Encoding.UTF8.GetBytes(concatnatedString);

                var hash = MD5.Create().ComputeHash(bytes);

                var metadata = new Metadata(fullName, length, creationTime, owner, bytes, hash);
               
                Dictionary.Add(file, metadata);
            }
        }

        public static string[] GetFileList()
        {
           return Dictionary.Keys.ToArray();
        }

        public static byte[] GetHashValue(string filePath)
        {
            Metadata metadata;
            Dictionary.TryGetValue(filePath, out metadata);
            return metadata.HashValue;
        }

        public static long GetLength(string filePath)
        {
            Metadata metadata;
            Dictionary.TryGetValue(filePath, out metadata);
            return metadata.Length;
        }
    }
}
