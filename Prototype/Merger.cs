﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Prototype
{
    class Merger
    {
        public static void Merge()
        {
            var files = FileHashDictionary.GetFileList();
            foreach (var file in files)
            {
                var info = Splitter.GetSplitInfo(file);
                MergeFile(file, info);
            }
        }

        private static void MergeFile(string filePath, IDictionary<Guid, int> info)
        {
            Console.Write("[{0}]\n  starts being merged...\n", filePath);

            var filename = Path.GetFileName(filePath);
            var mergeFilePath = string.Empty;
            if (filename != null)
            {
                mergeFilePath = Path.Combine(Constants.MergeFolderPath, filename);
            }

            if (string.IsNullOrEmpty(mergeFilePath)) return;

            using (var outputFile = new FileStream(mergeFilePath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                int cnt = 0;
                foreach (var pair in info)
                {
                    cnt++;
                    var splitFolderNumber = (pair.Value % Constants.FolderCount).ToString();
                    var splitFolderPath = Path.Combine(Constants.SplitFolderPath, splitFolderNumber);
                    var splitFilePath = Path.Combine(splitFolderPath, pair.Key.ToString());

                    var buffer = new byte[Constants.FileSize];

                    using (var inputSplitFile = new FileStream(splitFilePath, FileMode.Open, FileAccess.Read))
                    {
                        var bytesRead = 0;
                        while ((bytesRead = inputSplitFile.Read(buffer, 0, Constants.FileSize)) > 0)
                        {
                            if (cnt == info.Count)
                            {
                                var newSize = TrimEnd(buffer);
                                outputFile.Write(buffer, 0, newSize);
                            }
                            else 
                                outputFile.Write(buffer, 0, bytesRead);
                        }
                    }
                }
            }

            Console.WriteLine("  Done! Confirm the file on {0}", mergeFilePath); 
        }

        public static int TrimEnd(byte[] array)
        {
            int lastIndex = Array.FindLastIndex(array, b => b != 0);

            Array.Resize(ref array, lastIndex + 1);

            return lastIndex + 1;
        }
    }
}
