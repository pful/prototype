﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Prototype
{
    class Program
    {

        private static readonly string SplitLong = "SPLIT";
        private static readonly string SplitShort = "s";
        private static readonly string MergeLong = "MERGE";
        private static readonly string MergeShort = "m";
        private static readonly string ExitLong = "EXIT";

        private static Dictionary<string, Action<string[]>> lCommands =
            new Dictionary<string, Action<string[]>>(StringComparer.OrdinalIgnoreCase)
            {
                {SplitLong, SplitFunc},
                {SplitShort, SplitFunc},
                {MergeLong, MergeFunc},
                {MergeShort, MergeFunc}
            };

        private static void SplitFunc(string[] obj)
        {
            Splitter.Split();
        }

        private static void MergeFunc(string[] obj)
        {
            Merger.Merge();
        }

        public static void Main(string[] args)
        {
            Console.WriteLine("Put files you want to split on [{0}]", Constants.TargetFolderPath);

            while (true)
            {
                Console.Write("> ");
                string command = Console.ReadLine();

                string command_main = command.Split(' ').First();
                string[] arguments = command.Split(' ').Skip(1).ToArray();

                if (string.Equals(command_main, ExitLong, StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }

                if (lCommands.ContainsKey(command_main))
                {
                    Action<string[]> function_to_execute = null;
                    lCommands.TryGetValue(command_main, out function_to_execute);
                    function_to_execute?.Invoke(arguments);
                }
                else
                    Console.WriteLine("[!] Command '" + command_main + "' not found");
            }
            Console.WriteLine(@"Press any key to close this window.");
            Console.ReadLine();
        }
    }
    
}