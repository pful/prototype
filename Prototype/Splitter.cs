﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Prototype
{
    public class Splitter
    {
        private static readonly int FixedFileSize = Constants.FileSize;

        public static void Split()
        {
            var files = FileHashDictionary.GetFileList();
            foreach (var file in files)
            {
                var info = GetSplitInfo(file);
                SplitFile(file, info);
            }
        }
 
        public static IDictionary<Guid,int> GetSplitInfo(string filePath)
        {
            var splitInfo = new Dictionary<Guid, int>();
            var seed = FileHashDictionary.GetHashValue(filePath);
            var fileLength = FileHashDictionary.GetLength(filePath);
            var rand = new Random(BitConverter.ToInt32(seed, 0));
            var guid = new byte[16];

            var numOfFiles = (int) (fileLength/ FixedFileSize) + 1;
            for (var i = 0; i < numOfFiles; i++)
            {
                rand.NextBytes(guid);
                var filename = new Guid(guid);
                var idx = rand.Next();

                splitInfo.Add(filename, idx);
                //Console.WriteLine("{0} {1} {2}", filename, idx, idx % 5);
            }

            return splitInfo;
        }

        private static void SplitFile(string filePath, IDictionary<Guid, int> info)
        {
            Console.Write("[{0}]\n  starts being split...\n", filePath);

            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var paddingCount = (int)(info.Count * FixedFileSize - fs.Length);
                var cnt = 0;
                foreach(var pair in info)
                {
                    var splitFolderNumber = (pair.Value % Constants.FolderCount).ToString();
                    var splitFolderPath = Path.Combine(Constants.SplitFolderPath, splitFolderNumber);
                    var splitFilePath = Path.Combine(splitFolderPath, pair.Key.ToString());
                    using (var outputFile = new FileStream(splitFilePath, FileMode.Create, FileAccess.Write))
                    {
                        int byteRead;
                        var buffer = new byte[FixedFileSize];

                        if ((byteRead = fs.Read(buffer, 0, FixedFileSize)) <= 0) continue;

                        outputFile.Write(buffer, 0, byteRead);
                        cnt++;
                    }

                    if(cnt == info.Count)
                        using (var stream = new FileStream(splitFilePath, FileMode.Append))
                        {
                            var bytes = new byte[paddingCount];
                            Array.Clear(bytes, 0, paddingCount);
                            stream.Write(bytes, 0, bytes.Length);
                        }
                }

                Console.WriteLine("  It's split to {0} files", cnt);
            }
        }
    }
}
