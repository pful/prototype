﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public class CacheFileStream : AbstractStream
    {
        private string cachedFilePath;
        private FsItem item;
        private readonly bool writeable;
        private readonly FileStream stream;
        private readonly SemaphoreSlim sem = new SemaphoreSlim(1, 1);

        private bool closed;
        private bool written;
        private bool disposedValue;
       
        public Func<FsItem, Task> OnClosed { get; set; }
          
        public CacheFileStream(FsItem fsItem, string cachedFilePath, bool writeable)
        {
            item = fsItem;
            this.cachedFilePath = cachedFilePath;
            this.writeable = writeable;
            stream = new FileStream(this.cachedFilePath, FileMode.Open, writeable ? 
                FileAccess.ReadWrite : FileAccess.Read, FileShare.ReadWrite, 4096, true);
        }
          
        public override void Close()
        {
            if (closed)
            {
                return;
            }

            closed = true;

            item.Length = stream.Length;
            stream.Close();
            stream.Dispose();

            base.Close();

            if (written)
            {
                OnClosed(item);
            }
        }

        public override void Flush()
        {
            if(writeable)   
                stream.Flush();
        }

        public override void SetLength(long len)
        {
            sem.Wait();
            try
            {
                stream.SetLength(len);
                item.Length = len;
                written = true;
            }
            finally
            {
                sem.Release();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Close();
                    stream?.Dispose();
                    sem.Dispose();
                }

                disposedValue = true;
            }
        }

        public override async Task<int> Read(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            if (count == 0 || item.Length == 0) return 0;
            if (position >= item.Length) return 0;

            using (var timeoutcancel = new CancellationTokenSource(timeout))
            {
                try
                {
                    await sem.WaitAsync(timeoutcancel.Token);

                    int red;
                    try
                    {
                        stream.Position = position;
                        var toread = (int) Math.Min(count, item.Length - position);
                        if (toread < 0)
                        {
                            Log.Error("toread less than zero:" + item.Name);
                        }

                        red = await stream.ReadAsync(buffer, offset, toread, timeoutcancel.Token);
                    }
                    finally
                    {
                        sem.Release();
                    }

                    if (red == 0)
                    {
                        throw new Exception($"Red 0 File: {cachedFilePath} Len: {item.Length} Pos: {position}");
                    }

                    return red;
                }
                catch (TaskCanceledException ex)
                {
                    Log.Error("Timeout " + ex.Message);
                }
            }

            return 0;
        }

        public override async Task Write(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            if (!writeable)
            {
                throw new NotSupportedException();
            }

            using (var cancel = new CancellationTokenSource(timeout))
            {
                await sem.WaitAsync(cancel.Token);
                try
                {
                    stream.Position = position;
                    await stream.WriteAsync(buffer, offset, count, cancel.Token);
                }
                finally
                {
                    sem.Release();
                }
            }

            written = true;
            item.Length = stream.Length;
        }
        
        public static CacheFileStream OpenReadonly(FsItem fsItem, string cachedFilePath)
        {
            return new CacheFileStream(fsItem, cachedFilePath, false);
        }

        public static CacheFileStream OpenReadWrite(FsItem item, string path)
        {
            return new CacheFileStream(item, path, true);
        }
    }
}
