using System;
using System.IO;

namespace RemoteStorage
{
    public class Constants
    {
        public static readonly string RoamingFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

        public static readonly string RemoteStorage = "RemoteStorage";

        public static readonly string RemoteStorageAppPath = Path.Combine(RoamingFolder, RemoteStorage);
    }
}