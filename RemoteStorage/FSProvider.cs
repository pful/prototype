﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using DokanNet;
using FluentFTP;
using FileAccess = System.IO.FileAccess;

namespace RemoteStorage
{
    public class FSProvider : IFSProvider
    {
        private readonly FsItemsCache itemsCache = new FsItemsCache();

        private readonly FileCache _fileCache;

        private readonly Uploader _uploader;

        private readonly HashSet<string> excludedFiles = new HashSet<string>
        {
            "desktop.ini",
            "folder.jpg",
            "folder.gif",
            ".envolid"
        };

        public string FileSystemName => "Ftp";

        public string VolumeName { get; set; }

        public FSProvider(FtpClient client)
        {
            _provider = new FtpProvider(client);
            _fileCache = new FileCache(_provider);
            _uploader = new Uploader(_provider);

            _uploader.Start();
        }

        private readonly IRemoteStorageProvider _provider;

        public Task CreateDir(string filePath)
        {
            Log.Trace(this.GetType().Name, nameof(CreateDir), filePath);

            try
            {
                if (!Exists(filePath).Result)
                {
                    var newFolderBuilder =  _provider.Items.CreateFolder(filePath);
                    var dir = Path.GetDirectoryName(filePath);
                    if (string.IsNullOrEmpty(dir))
                        dir = "\\";
                    itemsCache.AddDirItem(dir, newFolderBuilder.Result.Build());               
                    itemsCache.Add(newFolderBuilder.Result.Build());
                    return newFolderBuilder;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return null;
            }
            return null;
        }

        public Task DeleteDir(string filePath)
        {
            Log.Trace(this.GetType().Name, nameof(DeleteDir), filePath);

            if (Exists(filePath).Result)
            {
                itemsCache.DeleteDir(filePath);
                return _provider.Items.Remove(filePath, true);
            }
            return null;
        }

        public Task DeleteFile(string filePath)
        {
            Log.Trace(this.GetType().Name, nameof(DeleteFile), filePath);

            if (Exists(filePath).Result)
            {
                itemsCache.DeleteFile(filePath);
                var cachedPath = Path.Combine(FileCache.CachePath, Path.GetFileName(filePath));
                if (File.Exists(cachedPath)) File.Delete(cachedPath);
                var sourcePath = Path.Combine(Uploader.CachePath, Path.GetFileName(filePath));
                if (File.Exists(sourcePath)) File.Delete(sourcePath);
                return _provider.Items.Remove(filePath);
            }
            return null;
        }
        
        public async Task MoveFile(string oldPath, string newPath, bool replace)
        {
            try
            {
                if (oldPath == newPath)
                    return;

                Log.Trace(this.GetType().Name, nameof(MoveFile), $"{oldPath} -> {newPath}");

                var oldDir = Path.GetDirectoryName(oldPath);
                var oldName = Path.GetFileName(oldPath);
                var newDir = Path.GetDirectoryName(newPath);
                var newName = Path.GetFileName(newPath);

                if (oldDir == null || newName == null)
                {
                    throw new InvalidOperationException("Both oldDir and newName should not be null.");
                }

                var item = await RetrieveItem(oldPath);

                if (oldName != newName)
                {
                    if (item.Length > 0 || item.IsDir)
                    {
                        item = (await _provider.Items.Rename(oldPath, newPath)).SetParentPath(oldDir).Build();
                    }
                    else
                    {
                        item = new FsItem(item)
                        {
                            Path = Path.Combine(oldDir, newName)
                        };
                    }

                    if (item == null)
                    {
                        throw new InvalidOperationException("Can not rename");
                    }
                }

                if (oldDir != newDir)
                {
                    var oldDirNodeTask = RetrieveItem(oldDir);
                    var newDirNodeTask = RetrieveItem(newDir);
                    Task.WaitAll(oldDirNodeTask, newDirNodeTask);
                    if (item.Length > 0 || item.IsDir)
                    {
                        item = _provider.Items.Move(oldPath, newPath, replace).Result.SetParentPath(newDir).Build();
                        if (item == null)
                        {
                            throw new InvalidOperationException("Can not move");
                        }
                    }
                    else
                    {
                        item = new FsItem(item)
                        {
                            Path = newPath
                        };
                    }
                }

                if (item.IsDir)
                {
                    itemsCache.MoveDir(oldPath, item);

                    if (Path.GetDirectoryName(item.Path) == "\\")
                    {
                        await _provider.UpdateRootChildren();
                    }
                }
                else
                {
                    itemsCache.MoveFile(oldPath, item);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

        }

        public async Task<IStream> OpenFile(string filePath, FileMode mode, FileAccess fileAccess, FileShare share, FileOptions options)
        {
            Log.Trace($"{this.GetType().Name}!! {nameof(OpenFile)} '{filePath}' /access: [{fileAccess}] share: [{share}] mode : {mode}");

            try
            {
                var item = await RetrieveItem(filePath);
           
                // FileAccess : Read ReadWrite Write

                // 2. FileAccess including [Write]
                // upload a new file to remote
                if (item == null && mode == FileMode.CreateNew)
                {
                    Log.Trace($"1.Opening {filePath} as New because mode:{mode}");
                    item = FsItem.MakeUploading(filePath, 0);
           
                    // get file stream through uploader
                    var newStream = _uploader.OpenNew(item);
                    itemsCache.Add(item);
           
                    return newStream;
                }
            
                // 1. Read FileAccess
                if (fileAccess == FileAccess.Read)
                {
                    Log.Trace($"Opening {filePath} for Read");

                    if (item == null) return null;
                    
                    if (!item.IsUploading)
                    {
                        return _fileCache.OpenReadWithDownload(item);
                    }

                    var resultStream = _fileCache.OpenReadCachedOnly(item);
                    if (resultStream != null)
                    {
                        return resultStream;
                    }

                    Console.WriteLine("OpenReadCachedOnly is null.");
                    await WaitForWhile(item, 25000);

                    return null;

                }

                // file-open with write access
                if (mode == FileMode.Create || mode == FileMode.Truncate)
                {
                    Log.Trace($"2.Opening {filePath} as Truncate because mode:{mode} and length {item.Length}");
                    item.Length = 0;
                    _fileCache.Delete(item);
                    item.MakeUploading();
                    var file = _uploader.OpenTruncate(item);

                    return file;
                }

                if (mode == FileMode.Open || mode == FileMode.OpenOrCreate || mode == FileMode.Append)
                {
                    Log.Trace($"3.Opening {filePath} as ReadWrite because mode:{mode} and length {item.Length}");
                    var file = _fileCache.OpenReadWrite(item); // todo name conflict
                    file.OnClosed = async fsItem =>
                    {
                        fsItem.LastWriteTime = DateTime.UtcNow;
                        if (!fsItem.IsUploading)
                        {
                            fsItem.MakeUploading();
                            var alreadyExisting = Path.Combine(FileCache.CachePath, fsItem.Name);
                            var newUploading = Path.Combine(Uploader.CachePath, fsItem.Name);

                            //if(File.Exists(newUploading))
                            //    File.Delete(newUploading);

                            try
                            {
                                File.Move(alreadyExisting, newUploading);
                            }
                            catch (Exception e)
                            {
                                Log.Error(e);
                            }
                        }
                        await _uploader.AddOverwrite(fsItem);
                    };
                    return file;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            return null;
        }

        private async Task WaitForWhile(FsItem item, int timeout)
        {
            var timeouttime = DateTime.UtcNow.AddMilliseconds(timeout);
            while (item.IsUploading)
            {
                if (DateTime.UtcNow > timeouttime)
                {
                    throw new TimeoutException();
                }

                await Task.Delay(1000);
                item = await RetrieveItem(item.Path);
            }
        }

        public async Task<bool> Exists(string filePath)
        {
            return await RetrieveItem(filePath) != null;
        }
        
        // retrieve file items 
        public async Task<FsItem> RetrieveItem(string itemPath)
        {
            Log.Trace(GetType().Name, nameof(RetrieveItem), itemPath);

            if (string.IsNullOrEmpty(itemPath) || itemPath == "\\")
            {
                return (await _provider.Items.GetRoot()).Build();
            }

            if (excludedFiles.Contains(Path.GetFileName(itemPath)))
            {
                return null;
            }

            // 1. already cached
            var cached = itemsCache.GetItem(itemPath);
            if (cached != null)
            {
                if (cached.NotExistingDummy) return null;
                return cached;
            }

      
            var folders = new LinkedList<string>();
            var currentPath = itemPath;
            FsItem item = null;
            do
            {
                folders.AddFirst(Path.GetFileName(currentPath));
                currentPath = Path.GetDirectoryName(currentPath);
                if (currentPath == "\\" || string.IsNullOrEmpty(currentPath))
                    break;
                item = itemsCache.GetItem(currentPath);
            } while (item == null);

            if (item == null)
                item = (await _provider.Items.GetRoot()).Build();

            if (currentPath == "\\")
                currentPath = string.Empty;

            foreach (var name in folders)
            {
                var newpath = currentPath + "\\" + name;
                var newnode = await _provider.Items.GetItem(newpath);
                if (newnode == null)
                {
                    itemsCache.AddItemOnly(FsItem.MakeNotExistingDummy(newpath));

                    Log.Error("NonExisting path from server: " + itemPath);
                    return null;
                }

                item = newnode.SetParentPath(
                    string.IsNullOrEmpty(currentPath)? "\\" : currentPath).Build();
                itemsCache.Add(item);
                currentPath = newpath;
            }

            return item;
        }

        public async Task<IList<FsItem>> GetDirItems(string folderPath)
        {
            Log.Trace(GetType().Name, nameof(GetDirItems), folderPath);

            var cached = itemsCache.GetDir(folderPath);
            if (cached != null)
            {
                Console.WriteLine("Got cached dir:\r\n  " + string.Join("\r\n  ", cached));
                return (await Task.WhenAll(cached.Select(RetrieveItem))).Where(i => i != null).ToList();
            }
        
            // no folder to be fetched
            var folderItem = await RetrieveItem(folderPath);
            var itemsBuilder = await _provider.Items.GetChildren(folderPath);
            var items = new List<FsItem>(itemsBuilder.Count);

            var currendDir = folderPath;
            if (currendDir == "\\")
                currendDir = string.Empty;

            foreach (var item in itemsBuilder)
            {
                items.Add(item.SetParentPath(currendDir).Build());
            }
            itemsCache.AddDirItems(folderPath, items);
            itemsCache.Add(folderItem);

            return items;

        }

        public Task<long> GetTotalFreeSpace()
        {
            return Task.FromResult(_totalSpace - _usedSpace);
        }
       

        public Task<long> GetTotalSize()
        {
            return Task.FromResult(_totalSpace);
        }

        private static long _totalSpace = 2L << 30;
        private static long _usedSpace;

        public async Task<long> GetTotalUsedSpace()
        {
            _usedSpace =  await CalculateFileSizeOnDir("\\");
            
            return _usedSpace;
        }

        private async Task<long> CalculateFileSizeOnDir(string path)
        {
            var items = await _provider.Items.GetChildren(path);
            long used = 0;
            foreach (var item in items)
            {
                var i = item.Build();
                if (i.IsDir)
                    used += await CalculateFileSizeOnDir(i.Path);
                else
                    used += i.Length;
            }
            
            return used;
        }

        private FileInformation _root;

        public async Task<FileInformation?> GetItemInfo(string fileName)
        {
            Log.Trace(GetType().Name, nameof(GetItemInfo), fileName);

            if (fileName == "\\" && 
                _root.CreationTime != null &&
                _root.LastAccessTime != null &&
                _root.LastWriteTime != null &&
                _root.FileName != null )
            {
                return _root;
            }

            var item = await RetrieveItem(fileName);
            if (item == null)
            {
                return null;
            }

            var result = new FileInformation
            {
                Length = item.Length,
                FileName = item.Name,
                Attributes = item.IsDir ? FileAttributes.Directory : FileAttributes.Normal,
                LastAccessTime = item.LastAccessTime,
                LastWriteTime = item.LastWriteTime,
                CreationTime = item.CreationTime
            };

            if (fileName == "\\" && _root.CreationTime == null)
            {
                _root = result;
            }

            return result;
        }

    }
}
