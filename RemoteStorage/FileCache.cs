﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public class FileCache
    {
        private static readonly string CacheFolder = "Cache";

        public static readonly string CachePath = Path.Combine(Constants.RemoteStorageAppPath, CacheFolder);

        private readonly IRemoteStorageProvider _provider;

        public FileCache(IRemoteStorageProvider provider)
        {
            _provider = provider;

            if (!Directory.Exists(CachePath))
            {
                Directory.CreateDirectory(CachePath);
            }
        }

        private bool MakeNewDownload(FsItem item, string path)
        {
            var fileInfo = new FileInfo(path);

            if (fileInfo.Exists && fileInfo.Length == item.Length)
            {
                return false;
            }

            Stream writer;
            if (!fileInfo.Exists || fileInfo.Length < item.Length)
            {
                writer = new FileStream(
                    path,
                    FileMode.Append,
                    FileAccess.Write,
                    FileShare.ReadWrite,
                    4096,
                    FileOptions.Asynchronous | FileOptions.SequentialScan);
            }
            else
            {
                writer = new FileStream(
                    path,
                    FileMode.Create,
                    FileAccess.Write,
                    FileShare.ReadWrite,
                    4096,
                    FileOptions.Asynchronous | FileOptions.SequentialScan);
            }

            // todo asynchrously
            Download(item, writer).Wait();

            return true;
        }
        public IStream OpenReadWithDownload(FsItem item)
        {
            var path = Path.Combine(CachePath, item.Name);

            if (MakeNewDownload(item, path))
            {
                return OpenReadCachedOnly(item);
            }

            return OpenReadCachedOnly(item);
        }

        public CacheFileStream OpenReadWrite(FsItem item)
        {
            var path = Path.Combine(CachePath, item.Name);
            MakeNewDownload(item, path);
            return CacheFileStream.OpenReadWrite(item, path);
        }

        private async Task Download(FsItem item, Stream stream)
        {
            Log.Trace($"Started download: {item.Name}");

            try
            {
                var buf = new byte[64 << 6]; // 64M
                var uncommitedSize = 0;
                const int commitSize = 512 << 6; // 512M
                using (stream)
                using (var writer = new BufferedStream(stream))
                {
                    while (writer.Length < item.Length)
                    {
                        var streamDownloaded = await _provider.Files.Download(item.Path);
                        //downloaded.Position = writer.Length;
                        int read;
                        do
                        {
                            read = await streamDownloaded.ReadAsync(buf, 0, buf.Length);

                            await writer.WriteAsync(buf, 0, read);
                            uncommitedSize += read;
                            if (uncommitedSize <= commitSize) continue;

                            uncommitedSize = 0;
                            await writer.FlushAsync();

                        } while (read > 0);
                    }
                    await writer.FlushAsync();
                }

                Log.Trace($"Completed download: {item.Name}");

            }
            catch (Exception e)
            {
                Log.Error($"Exception while downloading : {e.Message}\n{e.StackTrace}");
            }
        }

        public CacheFileStream OpenReadCachedOnly(FsItem item)
        {
            var path = Path.Combine(CachePath, item.Name);
       
            Log.Trace("Opened cached: " + item.Name);
            return CacheFileStream.OpenReadonly(item, path);
        }

        public void Delete(FsItem item)
        {
            try
            {
                var path = Path.Combine(CachePath, item.Name);
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception e)
            {
                Log.Error($"Exception while deleting file cache :{e.Message}\n{e.StackTrace}");
            }
        }
    }
}
