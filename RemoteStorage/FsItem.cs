﻿using System;
using System.Threading;

namespace RemoteStorage
{
    public class FsItem
    {
        private long length;

        public FsItem(FsItem item)
        {
            Path = item.Path;
            IsDir = item.IsDir;
            Length = item.Length;
            LastAccessTime = item.LastAccessTime;
            LastWriteTime = item.LastWriteTime;
            CreationTime = item.CreationTime;
        }

        public FsItem()
        {
        }

        public string Name => System.IO.Path.GetFileName(Path);

        public string Path { get; set; }

        public string ParentPath { get; set; }

        public DateTime CreationTime { get; set; }

        public string Dir => System.IO.Path.GetDirectoryName(Path);

        public DateTime FetchTime { get; } = DateTime.UtcNow;

        public byte[] Info { get; set; }

        public bool IsDir { get; internal set; }

        public bool IsUploading { get; internal set; }

        public DateTime LastAccessTime { get; set; }

        public DateTime LastWriteTime { get; set; }

        public long Length
        {
            get { return Interlocked.Read(ref length); }

            set { Interlocked.Exchange(ref length, value); }
        }

        public bool IsExpired(int expirationSeconds) => DateTime.UtcNow > FetchTime.AddSeconds(expirationSeconds);

        public static FsItem MakeUploading(string path, long length)
        {
            var now = DateTime.UtcNow;
            return new FsItem
            {
                IsUploading = true,
                Length = length,
                Path = path,
                IsDir = false,
                CreationTime = now,
                LastAccessTime = now,
                LastWriteTime = now,
            };
        }

        public void MakeUploading()
        {
            IsUploading = true;
        }

        public class Builder
        {
            public DateTime CreationTime { get; set; }

            public bool IsDir { get; set; }

            public DateTime LastAccessTime { get; set; }

            public DateTime LastWriteTime { get; set; }

            public long Length { get; set; }

            public string Name { get; set; }

            public string ParentPath { get; set; }

            public Builder SetParentPath(string path)
            {
                if (path != null)
                {
                    ParentPath = path.StartsWith("\\") ? path : "\\" + path;
               }

                return this;
            }

            public Builder() { }

            public FsItem Build()
            {
                if (ParentPath == null)
                {
                    ParentPath = string.Empty;
                }

                var result = new FsItem
                {
                    CreationTime = CreationTime,
                    IsDir = IsDir,
                    LastAccessTime = LastAccessTime,
                    LastWriteTime = LastWriteTime,
                    Length = Length,
                    Path = ParentPath != string.Empty ? System.IO.Path.Combine(ParentPath, Name) : "\\" + Name                 
                };

                return result;
            }

            public FsItem BuildRoot()
            {
                ParentPath = "\\";
                Name = string.Empty;
                return Build();
            }
        }

        // To replacement to cache files in path that does not exist
        public bool NotExistingDummy { get; private set; }

        public static FsItem MakeNotExistingDummy(string path)
        {
            return new FsItem
            {
                Path = path,
                NotExistingDummy = true
            };
        }
        
    }
}
