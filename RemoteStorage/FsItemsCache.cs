﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public class FsItemsCache
    {
        private readonly CancellationTokenSource cancellation = new CancellationTokenSource();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private readonly Dictionary<string, DirItem> pathToDirItem = new Dictionary<string, DirItem>();
        private readonly Dictionary<string, FsItem> pathToNode = new Dictionary<string, FsItem>();
        private bool disposedValue; // To detect redundant calls

        public int FSItemsExpirationSeconds { get; set; } = 5 * 60;

        public int DirItemsExpirationSeconds { get; set; } = 60;

        public FsItemsCache()
        {
            Task.Factory.StartNew(async () => await Clean(), TaskCreationOptions.LongRunning);
        }

        private async Task Clean()
        {
            var token = cancellation.Token;

            while (!token.IsCancellationRequested && !disposedValue)
            {
                try
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        foreach (var key in pathToNode.Where(p => p.Value.IsExpired(FSItemsExpirationSeconds)).Select(p => p.Key).ToList())
                        {
                            pathToNode.Remove(key);
                        }
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }

                    _lock.EnterWriteLock();
                    try
                    {
                        foreach (var key in pathToDirItem.Where(p => p.Value.IsExpired).Select(p => p.Key).ToList())
                        {
                            pathToNode.Remove(key);
                        }
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }

                await Task.Delay(FSItemsExpirationSeconds * 6, token);
            }
        }

        private class DirItem
        {
            public DirItem(string item, int expirationSeconds)
            {
                Items = new HashSet<string> {item};
                ExpirationTime = DateTime.UtcNow.AddSeconds(expirationSeconds);
            }

            public DirItem(IEnumerable<string> items, int expirationSeconds)
            {
                Items = new HashSet<string>(items);
                ExpirationTime = DateTime.UtcNow.AddSeconds(expirationSeconds);
            }

            public bool IsExpired => DateTime.UtcNow > ExpirationTime;

            public HashSet<string> Items { get; }

            private DateTime ExpirationTime { get; }
        }

        public FsItem GetItem(string filePath)
        {
            _lock.EnterUpgradeableReadLock();
            try
            {
                FsItem item;
                if (!pathToNode.TryGetValue(filePath, out item))
                {
                    return null;
                }

                if (!item.IsUploading && item.IsExpired(FSItemsExpirationSeconds))
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        pathToNode.Remove(filePath);
                        return null;
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }

                return item;
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }
        }

        public void Add(FsItem item)
        {
            _lock.EnterWriteLock();
            try
            {
                pathToNode[item.Path] = item;
                DirItem dirItem;
                if (pathToDirItem.TryGetValue(item.Dir, out dirItem))
                {
                    dirItem.Items.Add(item.Path);
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void AddDirItems(string folderPath, IList<FsItem> items)
        {
            _lock.EnterWriteLock();
            try
            {
                pathToDirItem[folderPath] = new DirItem(items.Select(i => i.Path).ToList(), DirItemsExpirationSeconds);
                foreach (var item in items)
                {
                    pathToNode[item.Path] = item;
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void AddDirItem(string folderPath, FsItem item)
        {
            _lock.EnterWriteLock();
            try
            {
                var dirItem = pathToDirItem[folderPath];
                if (dirItem == null)
                {
                    dirItem = new DirItem(item.Path, DirItemsExpirationSeconds);
                }
                else
                {
                    dirItem.Items.Add(item.Path);
                }

                pathToDirItem[folderPath] = dirItem; 
                pathToNode[item.Path] = item;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public IEnumerable<string> GetDir(string filePath)
        {
            _lock.EnterUpgradeableReadLock();
            try
            {
                DirItem item;
                if (!pathToDirItem.TryGetValue(filePath, out item))
                {
                    return null;
                }

                if (!item.IsExpired)
                {
                    return item.Items;
                }

                _lock.EnterWriteLock();
                try
                {
                    pathToDirItem.Remove(filePath);
                    return null;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }
            finally
            {
                _lock.ExitUpgradeableReadLock();
            }
        }

        public void DeleteFile(string filePath)
        {
            _lock.EnterWriteLock();
            try
            {
                var dirPath = Path.GetDirectoryName(filePath);
                if (dirPath == null)
                {
                    throw new InvalidOperationException($"dirPath is null for '{filePath}'");
                }
                DirItem dirItem;
                if (pathToDirItem.TryGetValue(dirPath, out dirItem))
                {
                    dirItem.Items.Remove(filePath);
                }
                pathToNode.Remove(filePath);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void DeleteDir(string filePath)
        {
            _lock.EnterWriteLock();
            try
            {
                var dirPath = Path.GetDirectoryName(filePath);
                if (dirPath == null)
                {
                    throw new InvalidOperationException($"dirPath is null for '{filePath}'");
                }

                DirItem dirItem;
                if (pathToDirItem.TryGetValue(dirPath, out dirItem))
                {
                    dirItem.Items.RemoveWhere(i => i == filePath);
                }

                foreach (var key in pathToNode.Keys.Where(v => v.StartsWith(filePath, StringComparison.InvariantCulture)).ToList())
                {
                    pathToNode.Remove(key);
                }

                foreach (var key in pathToDirItem.Keys.Where(v => v.StartsWith(filePath, StringComparison.InvariantCulture)).ToList())
                {
                    pathToDirItem.Remove(key);
                }
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void AddItemOnly(FsItem item)
        {
            _lock.EnterWriteLock();
            try
            {
                pathToNode[item.Path] = item;
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void MoveDir(string oldPath, FsItem newNode)
        {
            _lock.EnterWriteLock();
            try
            {
                DeleteDir(oldPath);
                Add(newNode);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        public void MoveFile(string oldPath, FsItem newNode)
        {
            _lock.EnterWriteLock();
            try
            {
                DeleteFile(oldPath);
                Add(newNode);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

  
    }
}
