﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Threading.Tasks;

using FluentFTP;

namespace RemoteStorage
{
    public class FtpProvider : IRemoteStorageProvider, IRemoteStorageFiles, IRemoteStorageItems
    {
        public string ServiceName => "Ftp";

        public IRemoteStorageFiles Files => this;

        public IRemoteStorageItems Items => this;

        public bool ShouldUpdateRootChildren { get; set; }

        private readonly FtpClient _ftpClient;

        public FtpProvider(FtpClient ftpClient)
        {
            _ftpClient = ftpClient;
        }

        public Task<Stream> Download(string path)
        {
            return _ftpClient.OpenReadAsync(path);
        }

        public Task<FsItem.Builder> Overwrite(string path, Func<FileStream> f)
        {
            return UploadNew(path, f);
        }
        
        public async Task<FsItem.Builder> UploadNew(string path, Func<FileStream> f)
        {
            Contract.Requires(_ftpClient != null, "FTP client should not be null.");
            Contract.Requires(_ftpClient.IsConnected, "Connection should be available.");
            
            try
            {
                using (var stream = f())
                {
                    var result = await _ftpClient.UploadAsync(stream, path, FtpExists.Overwrite);
                    if (result)
                    {
                        var item = await _ftpClient.GetObjectInfoAsync(path);//await GetItem(path);
                        if (item != null) Console.WriteLine($"FtpProvider did upload successfully: {path}");
                        return item == null ? null : FromRemoteItem(item);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }

            Console.WriteLine("Unsuccessfully Upload Finished");
            return null;
        }

        private FtpListItem _root;
        private FsItem _rootItem;
        
        // todo rename make changes to the list.
        private List<FtpListItem> _rootChildren;
        private List<FsItem.Builder> _rootChildrenItems;

        public async Task<FsItem.Builder> GetRoot()
        {
            if (_root != null && _rootItem != null) return FromRemoteItem(_root);
            _root = await _ftpClient.GetObjectInfoAsync("\\");
            _rootItem = FromRemoteItem(_root).Build();
            if (_rootChildren == null)
            {
                _rootChildren = new List<FtpListItem>();
                _rootChildrenItems = new List<FsItem.Builder>();
                var list = await _ftpClient.GetListingAsync("\\");
                foreach (var item in list)
                {
                    _rootChildren.Add(item);
                    _rootChildrenItems.Add(FromRemoteItem(item).SetParentPath("\\"));
                }
            }
            return FromRemoteItem(_root);
        }

        public async Task<FsItem.Builder> GetItem(string path)
        {
            var item = await _ftpClient.GetObjectInfoAsync(path);
            return item == null? null : FromRemoteItem(item);
        }

        public static FsItem.Builder FromRemoteItem(FtpListItem item)
        {
            return new FsItem.Builder
            {
                Length = item.Size,
                IsDir = item.Type == FtpFileSystemObjectType.Directory,
                CreationTime = item.Created,
                LastAccessTime = item.Modified,
                LastWriteTime = item.Modified,
                Name = item.Name
            };
        }

        public async Task<IList<FsItem.Builder>> UpdateRootChildren()
        {
            var items = await _ftpClient.GetListingAsync("\\");
            var children = new List<FsItem.Builder>(items.Length);

            foreach (var i in items)
            {
                var item = FromRemoteItem(i).SetParentPath("\\");
                children.Add(item);
            }

            _rootChildrenItems = children;
            return children;
        }

        public async Task<IList<FsItem.Builder>> GetRootChildren()
        {
            if (_rootChildrenItems != null || ShouldUpdateRootChildren)
                await UpdateRootChildren();

            if (_rootChildren != null && _rootChildrenItems != null)
                return _rootChildrenItems;

            return null;
        }

        public async Task<IList<FsItem.Builder>> GetChildren(string path)
        {
            var items = await _ftpClient.GetListingAsync(path);
            var children = new List<FsItem.Builder>(items.Length);

            foreach (var i in items)
            {
                var item = FromRemoteItem(i).SetParentPath(path);
                children.Add(item);
            }

            return children;
        }

        public async Task<FsItem.Builder> CreateFolder(string path)
        {
            await _ftpClient.CreateDirectoryAsync(path);
            var item = await _ftpClient.GetObjectInfoAsync(path);
            return FromRemoteItem(item);
        }

        public async Task<FsItem.Builder> Move(string @from, string to, bool replace)
        {
            FsItem.Builder item = null;
            if (_ftpClient.DirectoryExists(@from))
            {
                await _ftpClient.MoveDirectoryAsync(@from, to,
                    replace ? FtpExists.Overwrite : FtpExists.Append);

                item = FromRemoteItem(await _ftpClient.GetObjectInfoAsync(to));

            }
            else if (_ftpClient.FileExists(@from))
            {
                await _ftpClient.MoveFileAsync(@from, to,
                    replace ? FtpExists.Overwrite : FtpExists.Append);

                item = FromRemoteItem(await _ftpClient.GetObjectInfoAsync(to));
            }

            return item;
        }

        public Task<FsItem.Builder> Rename(string @from, string to)
        {
            return Move(@from, to, true);
        }

        public Task Remove(string path, bool isDir = false)
        {
            if(isDir)
                return _ftpClient.DeleteDirectoryAsync(path);

            return _ftpClient.DeleteFileAsync(path);
        }
    }
}
