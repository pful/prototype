﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DokanNet;
using FileAccess = System.IO.FileAccess;

namespace RemoteStorage
{
    public interface IFSProvider
    {
        string FileSystemName { get; }

        string VolumeName { get; set; }

        Task CreateDir(string filePath);

        Task DeleteDir(string filePath);

        Task DeleteFile(string filePath);

        Task MoveFile(string oldPath, string newPath, bool replace);

        Task<IStream> OpenFile(string filePath, FileMode mode, FileAccess fileAccess, FileShare share, FileOptions options);

        Task<bool> Exists(string filePath);

        Task<FsItem> RetrieveItem(string itemPath);

        Task<IList<FsItem>> GetDirItems(string folderPath);

        Task<long> GetTotalFreeSpace();

        Task<long> GetTotalSize();

        Task<long> GetTotalUsedSpace();

        Task<FileInformation?> GetItemInfo(string fileName);
    }
}
