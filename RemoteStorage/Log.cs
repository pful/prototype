﻿using System;
using System.IO;
using System.Linq;

using DokanNet;
using DokanNet.Logging;
using FileAccess = DokanNet.FileAccess;
using static DokanNet.FormatProviders;

namespace RemoteStorage
{
    public static class Log
    {
        private static readonly ConsoleLogger Logger = new ConsoleLogger();

        public static NtStatus Trace(string method, string fileName, DokanFileInfo info, NtStatus result,
            params object[] parameters)
        {
#if TRACE
            var extraParameters = parameters != null && parameters.Length > 0
                ? ", " + string.Join(", ", parameters.Select(x => string.Format(DefaultFormatProvider, "{0}", x)))
                : string.Empty;

            Logger.Debug(DokanFormat($"{method}('{fileName}', {info}{extraParameters}) -> {result}"));
#endif

            return result;
        }

        public static void Trace(string method, string fileName, DokanFileInfo info,
            FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes)
        {
#if TRACE
            Logger.Debug(
                DokanFormat(
                    $"{method}('{fileName}', {info}, [{access}], [{share}], [{mode}], [{options}], [{attributes}])"));
#endif

        }

        public static NtStatus Trace(string method, string fileName, DokanFileInfo info,
            FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes,
            NtStatus result)
        {
#if TRACE
            Logger.Debug(
                DokanFormat(
                    $"{method}('{fileName}', {info}, [{access}], [{share}], [{mode}], [{options}], [{attributes}]) -> {result}"));
#endif

            return result;
        }

        public static void Trace(string message)
        {
            Logger.Debug(message);
        }

        public static void Trace(string className, string method, string path)
        {
            Logger.Debug(
                DokanFormat($"{className}/{method}(filepath: {path})"));
        }

        public static void Trace(string className, string method, DokanFileInfo info)
        {
            Logger.Debug(
                DokanFormat($"{className}/{method}(info: {info})"));
        }

        public static void Trace(string className, string method, string path, DokanFileInfo info)
        {
            Logger.Debug(
                DokanFormat($"{className}/{method}(filepath: {path}, info: {info})"));
        }

        public static void Trace(string className, string method, string fileName,
            System.IO.FileAccess access, FileShare share, FileMode mode, FileOptions options)
        {
#if TRACE
            Logger.Debug(
                DokanFormat(
                    $"{method}('{fileName}', [{access}], [{share}], [{mode}], [{options}])"));
#endif
            
        }

        public static void Error(string s)
        {
            Logger.Error(s);
        }

        public static void Error(Exception exception)
        {
            Logger.Error($"{exception.Message} \n {exception.StackTrace}");
        }

        public static void Error(string s, Exception exception)
        {
            Logger.Error($"{s} {exception.Message} \n {exception.StackTrace}");
        }

        //        private string Format(string method, string fileName, DokanFileInfo info,
        //            FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes)
        //        {
        //            return DokanFormat(
        //                $"{method}('{fileName}', {info}, [{access}], [{share}], [{mode}], [{options}], [{attributes}])");
        //        }

        //        private string Format(string method, string fileName, DokanFileInfo info,
        //            FileAccess access, FileShare share, FileMode mode, FileOptions options, FileAttributes attributes,
        //            NtStatus result)
        //        {
        //            return DokanFormat(
        //                $"{method}('{fileName}', {info}, [{access}], [{share}], [{mode}], [{options}], [{attributes}]) -> {result}");
        //        }
        
    }
}
