﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public class NewFileWriter : AbstractStream
    {
        private readonly FileStream stream;
        private readonly SemaphoreSlim sem = new SemaphoreSlim(1, 1);
        private bool closed;
        private bool disposedValue;

        private readonly FsItem item;

        public string UploadCachePath { get; }

        public bool Cancelled { get; private set; }
        
        public NewFileWriter(FsItem item, string path)
        {
            this.item = item;
            UploadCachePath = path;
            if(File.Exists(path)) File.Delete(path);
            stream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite, 4096, true);
        }

        public void CancelUpload()
        {
            Cancelled = true;
        }
       
        public override void Close()
        {
            if (closed)
            {
                return;
            }

            closed = true;

            item.Length = stream.Length;
            stream.Close();
            stream.Dispose();

            Log.Trace($"Closed New file: {item.Path} of {item.Length} bytes");
            base.Close();
        }

        public override void Flush()
        {
            stream.Flush();
        }

        public override void SetLength(long len)
        {
            sem.Wait();
            try
            {
                stream.SetLength(len);
            }
            finally
            {
                sem.Release();
            }

            item.Length = len;
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Close();
                    stream.Dispose();
                    sem.Dispose();
                }

                disposedValue = true;
            }
        }

        public override async Task<int> Read(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            await sem.WaitAsync();
            try
            {
                stream.Position = position;
                return await stream.ReadAsync(buffer, offset, count);
            }
            finally
            {
                sem.Release();
            }
        }

        public override async Task Write(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            await sem.WaitAsync();
            try
            {
                // if (lastPosition != position) Log.Warn($"Write Position in New file was changed from {lastPosition} to {position}");
                stream.Position = position;
                await stream.WriteAsync(buffer, offset, count);

                //item.Length = stream.Length;
            }
            finally
            {
                sem.Release();
            }
        }
    }
}
