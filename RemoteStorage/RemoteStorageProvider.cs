﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public interface IRemoteStorageProvider
    {
        string ServiceName { get; }

        IRemoteStorageFiles Files { get; }

        IRemoteStorageItems Items { get; }

        Task<IList<FsItem.Builder>> UpdateRootChildren();
    }

    public interface IRemoteStorageFiles
    {
        Task<Stream> Download(string path);

        Task<FsItem.Builder> Overwrite(string path, Func<FileStream> f);

        Task<FsItem.Builder> UploadNew(string path, Func<FileStream> f);
    }

    public interface IRemoteStorageItems
    {
        Task<FsItem.Builder> GetRoot();

        Task<IList<FsItem.Builder>> GetRootChildren();

        Task<FsItem.Builder> GetItem(string path);

        Task<IList<FsItem.Builder>> GetChildren(string path);

        //Task<bool> Exists(string path);

        Task<FsItem.Builder> CreateFolder(string path);

        Task<FsItem.Builder> Move(string from, string to, bool replace);

        Task<FsItem.Builder> Rename(string from, string newName);

        Task Remove(string path, bool isDir = false);
        
    }
}
