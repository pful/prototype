﻿using System;
using System.Net;

using DokanNet;
using FluentFTP;

namespace RemoteStorage
{
    public class RemoteStorageRunner
    {
        private static FtpClient _client;
        private static VirtualDrive _virtualDrive;
        private const string MountPoint = "T:\\";
        private const char DriveLetter = 'T';

        public static bool IsConnected { private set; get; }

        public static void Start()
        {
            try
            {
                _client = GetFtpClient();

                if (_client == null)
                {
                    Console.WriteLine(@"\nCannot connect to FTP server.");
                    Console.ReadLine();
                    return;
                }

                IsConnected = _client.IsConnected;

                _virtualDrive = new VirtualDrive(new FSProvider(_client) { VolumeName = "Ftp" });
                _virtualDrive.Mount(MountPoint,/* DokanOptions.AltStream |*/ DokanOptions.FixedDrive | DokanOptions.DebugMode, 1);
                
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(@"Error: " + e.Message);
            }
            catch (DokanException ex)
            {
                Console.WriteLine(@"Error: " + ex.Message);
            }
        }

        private static FtpClient GetFtpClient()
        {
            try
            {
                var client = new FtpClient("127.0.0.1")
                {
                    Credentials = new NetworkCredential("user", "1234"),
                    ReadTimeout = 30000,
                    DataConnectionReadTimeout = 30000,
                    SocketKeepAlive = true,
                    EnableThreadSafeDataConnections = true
                };
                client.Connect();
                return client;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return null;
        }

        public static void Stop()
        {
            if (!_client.IsConnected) return;
            _client.Disconnect();
            _client.Dispose();

            Dokan.Unmount(DriveLetter);
            Dokan.RemoveMountPoint(MountPoint);

            Log.Trace($"{nameof(Start)} stoped");
        }
    }
}
