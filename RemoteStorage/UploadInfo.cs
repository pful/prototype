﻿using System;
using System.Threading;

namespace RemoteStorage
{
    public class UploadInfo : IDisposable
    {
        private bool disposedValue;

        //public UploadInfo()
        //{
        //}

        public UploadInfo(FsItem item)
        {
            Name = item.Name;
            Length = item.Length;
            SourcePath = System.IO.Path.Combine(Uploader.CachePath, item.Name);
            TargetPath = item.Path;
        }

        public CancellationTokenSource Cancellation { get; } = new CancellationTokenSource();

        public string FailReason { get; set; }

        public long Length { get; set; }

        public bool Overwrite { get; set; } = false;

        //public string ParentId { get; set; }

        public string Name { get; }

        public string SourcePath { get; set; }

        /// <summary>
        /// Gets or sets original remote file path
        /// </summary>
        public string TargetPath { get; }

        
        public void Dispose()
        {
            Dispose(true);
        }

        internal FsItem ToFSItem()
        {
            var result = new FsItem.Builder()
            {
                Name = System.IO.Path.GetFileName(TargetPath),
                Length = Length,
                ParentPath = System.IO.Path.GetDirectoryName(TargetPath)
            }.Build();
            result.MakeUploading();
            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Cancellation.Dispose();
                }

                disposedValue = true;
            }
        }
    }
}
