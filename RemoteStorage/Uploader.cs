﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteStorage
{
    public class Uploader
    {
        private static readonly string UploadFolderName = "Upload";
   
        public static readonly string CachePath = Path.Combine(Constants.RemoteStorageAppPath, UploadFolderName);

        private readonly CancellationTokenSource cancellation = new CancellationTokenSource();

        private readonly SemaphoreSlim sem = new SemaphoreSlim(1);

        private bool disposedValue; // To detect redundant calls

        private Task _uploadTask;

        private readonly BlockingCollection<UploadInfo> _uploadList = new BlockingCollection<UploadInfo>();

        private readonly IRemoteStorageProvider _provider;

        public Uploader(IRemoteStorageProvider provider)
        {
            _provider = provider;
            if (!Directory.Exists(CachePath))
                Directory.CreateDirectory(CachePath);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Stop();
                    cancellation.Dispose();
                    sem.Dispose();
                    _uploadList.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Start()
        {
            if (_uploadTask != null)
            {
                return;
            }

            _uploadTask = Task.Factory.StartNew(UploadTask, cancellation.Token,
                TaskCreationOptions.LongRunning | TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        public void Stop()
        {
            if (_uploadTask == null)
            {
                return;
            }

            cancellation.Cancel();
            try
            {
                _uploadTask.Wait();
            }
            catch (AggregateException e)
            {
                e.Handle(exception => exception is TaskCanceledException);
            }
            _uploadTask = null;
        }

        private void UploadTask()
        {
            try
            {
                {
                    UploadInfo info;
                    while (_uploadList.TryTake(out info, -1, cancellation.Token))
                    {
                        var infoCopy = info;
                        if (!sem.Wait(-1, cancellation.Token))
                        {
                            return;
                        }
                        
                        Task.Factory.StartNew(async () =>
                        {
                            try
                            {
                                await Upload(infoCopy);
                            }
                            catch (Exception e)
                            {
                                Log.Error(e);
                            }
                        }, TaskCreationOptions.LongRunning);
                    }
                    Thread.Sleep(30);
                }
            }
            catch (OperationCanceledException)
            {
                Log.Error("Upload Task Stopped");
            }
        }

        private async Task Upload(UploadInfo info)
        {
            try
            {
                if (!info.Overwrite)
                {
                    Log.Trace($"Uploader, Upload {info.TargetPath}");

                    var parentPath = Path.GetDirectoryName(info.TargetPath);
                    if (string.CompareOrdinal(parentPath, "\\") != 0)
                    {
                        var parentDir = await _provider.Items.GetItem(parentPath);
               
                        if (parentDir == null || !parentDir.IsDir)
                        {
                            Log.Error("Folder does not exist to upload file: " + info.TargetPath);
                            CleanUpload(info);
                            return;
                        }
                    }

                    /*
                    var target = await _provider.Items.GetItem(info.TargetPath);
                    if (target != null)
                    {
                        Log.Error("File already exists on Remote: " + info.TargetPath);
                        CleanUpload(info);
                        return;
                    }
                    */

                    var uploadResult = await _provider.Files.UploadNew(info.TargetPath,
                        () => new FileStream(info.SourcePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
                            true));
              
                    if (uploadResult == null)
                    {
                        Log.Error("File is not successfully uploaded: " + info.TargetPath);
                        bool added;
                        do
                        {
                            added = _uploadList.TryAdd(info);
                        } while (!added);
                        Log.Error("Failed file added again: " + info.TargetPath);
                    }
                }
                else
                {
                    Log.Trace($"Uploader, Overwrite {info.TargetPath}");

                    var target = await _provider.Items.GetItem(info.TargetPath);
                    if (target == null)
                    {
                        Log.Error("File to be overwritten does not exists: " + info.TargetPath);
                        CleanUpload(info);
                        return;
                    }
                    var uploadResult = await _provider.Files.Overwrite(info.TargetPath,
                        () => new FileStream(info.SourcePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
                            true));

                    if (uploadResult == null)
                    {
                        Log.Error("File is not successfully uploaded: " + info.TargetPath);
                        bool added;
                        do
                        {
                            added = _uploadList.TryAdd(info);
                        } while (!added);
                        Log.Error("Failed file added again: " + info.TargetPath);
                    }
                }

                CleanUpload(info);
            }
            catch (FileNotFoundException)
            {
                Log.Error("Upload did not find the file");
            }
            catch (Exception e)
            {
                bool added;
                do
                {
                    added = _uploadList.TryAdd(info);
                } while (!added);
                Log.Error($"Upload Exception: {e.Message}: \n{e.StackTrace}");
            }
            finally
            {
                sem.Release();
            }
        }

        private void CleanUpload(UploadInfo info)
        {
            if (info.SourcePath != null)
            {
                try
                {
                    File.Delete(info.SourcePath);
                }
                catch (Exception)
                {
                    Log.Error("CleanUpload did not find the file, probably successfully moved");
                }
            }
            
            info.Dispose();
        }

        public IStream OpenNew(FsItem item)
        {
            var path = Path.Combine(CachePath, item.Name);
            var newWriter = new NewFileWriter(item, path);
            newWriter.OnClose = async () =>
            {
                if (!newWriter.Cancelled)
                {
                    await AddUpload(item);
                }
            };
            return newWriter;
        }

        private async Task AddUpload(FsItem item)
        {
            var info = new UploadInfo(item);
            _uploadList.Add(info);
        }

        public IStream OpenTruncate(FsItem item)
        {
            var path = Path.Combine(CachePath, item.Name);
            var newWriter = new NewFileWriter(item, path);
            newWriter.SetLength(0);
            newWriter.OnClose = async () =>
            {
                if (!newWriter.Cancelled)
                    await AddOverwrite(item);
            };
            return newWriter;
        }

        internal async Task AddOverwrite(FsItem item)
        {
            var info = new UploadInfo(item) {Overwrite = true};
            _uploadList.Add(info);
        }
    }
}
