﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;

using DokanNet;
using FileAccess = DokanNet.FileAccess;

namespace RemoteStorage
{
    internal class VirtualDrive : IDokanOperations
    {
        private const FileAccess DataAccess = FileAccess.ReadData | FileAccess.WriteData | FileAccess.AppendData |
                                              FileAccess.Execute |
                                              FileAccess.GenericExecute | FileAccess.GenericWrite | FileAccess.GenericRead;

        private const FileAccess DataReadAccess = FileAccess.ReadData | FileAccess.GenericExecute |
                                                  FileAccess.Execute;

        private const FileAccess DataWriteAccess = FileAccess.WriteData | FileAccess.AppendData |
                                                   FileAccess.Delete |
                                                   FileAccess.GenericWrite;

        private const int ReadTimeout = 30000;

        private readonly string creator = WindowsIdentity.GetCurrent().Name;

        private readonly IFSProvider provider;

#if TRACE
        private string lastFilePath;
#endif


        public VirtualDrive(IFSProvider provider)
        {
            this.provider = provider;
        }

        public string MountPath { get; internal set; }

        public bool ReadOnly { get; internal set; }

        private static T Wait<T>(Task<T> task, int timeout = ReadTimeout)
        {
            if (!task.Wait(timeout))
            {
                throw new AggregateException(new TimeoutException());
            }

            return task.Result;
        }

        private static void Wait(Task task, int timeout = ReadTimeout)
        {
            if (!task.Wait(timeout))
            {
                throw new AggregateException(new TimeoutException());
            }
        }

        public NtStatus CreateFile(string fileName, FileAccess access, FileShare share, FileMode mode, FileOptions options,
            FileAttributes attributes, DokanFileInfo info)
        {
            Log.Trace(nameof(CreateFile), fileName, info, access, share, mode, options, attributes);

            try
            {
                var res = Wait(MainCreateFile(fileName, access, share, mode, options, info));
#if TRACE
                var readWriteAttributes = (access & DataAccess) == 0;
                if (!(readWriteAttributes || info.IsDirectory) || (res != DokanResult.Success && !(lastFilePath == fileName && res == DokanResult.FileNotFound)))
                {
                    if (!(info.Context is IStream))
                    {
                        Log.Trace($"{fileName}\r\n  Access:[{access}]\r\n  Share:[{share}]\r\n  Mode:[{mode}]\r\n  Options:[{options}]\r\n  Attr:[{attributes}]\r\nStatus:{res}");
                    }

                    lastFilePath = fileName;
                }
#endif
                //return Log.Trace(nameof(CreateFile), fileName, info, access, share, mode, options, attributes, res);
                return res;
            }
            catch (Exception e) when (e.InnerException is FileNotFoundException)
            {
                Log.Error($"{nameof(CreateFile)} - File not found: {fileName}\r\n  Access:[{access}]\r\n  Share:[{share}]\r\n  Mode:[{mode}]\r\n  Options:[{options}]\r\n  Attr:[{attributes}]", e); ;
                return DokanResult.FileNotFound;
            }
            catch (Exception e) when (e.InnerException is TimeoutException)
            {
                Log.Error($"{nameof(CreateFile)} - Timeout: {fileName}\r\n  Access:[{access}]\r\n  Share:[{share}]\r\n  Mode:[{mode}]\r\n  Options:[{options}]\r\n  Attr:[{attributes}]", e);
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error($"{nameof(CreateFile)} - Unexpected exception: {fileName}\r\n  Access:[{access}]\r\n  Share:[{share}]\r\n  Mode:[{mode}]\r\n  Options:[{options}]\r\n  Attr:[{attributes}]", e);
                if (e.InnerException != null)
                {
                    var ei = e.InnerException;
                    Log.Error($"{nameof(CreateFile)} - Inner Exception: ", ei);

                }
                return DokanResult.Error;
            }
        }

        private async Task<NtStatus> MainCreateFile(string fileName, FileAccess access, FileShare share, 
            FileMode mode, FileOptions options, DokanFileInfo info)
        {
            Console.WriteLine($"{nameof(MainCreateFile)} {fileName}");

            if (!HasAccess(info))
            {
                return DokanResult.AccessDenied;
            }

            
            if (info.IsDirectory /*|| item != null && item.IsDir*/)
            {
                if (mode == FileMode.CreateNew)
                {
                    return MainCreateDirectory(fileName);
                }

                if (mode == FileMode.Open)
                {
                    if (string.CompareOrdinal("\\", fileName) != 0 && !await provider.Exists(fileName))
                        return DokanResult.PathNotFound;
                }

                if (access == FileAccess.Synchronize)
                {
                    info.Context = new object();
                    return DokanResult.Success;
                }

                info.Context = new object();
                return DokanResult.Success;
            }

            Console.WriteLine($"{nameof(MainCreateFile)} {fileName} calls provider.RetrieveItem");
            var item = await provider.RetrieveItem(fileName);

            var readWriteAttributes = (access & DataAccess) == 0;
            switch (mode)
            {
                case FileMode.Open:
                    if (item == null) 
                    {
                        return DokanResult.FileNotFound;
                    }

                    // check if driver only wants to read attributes, security info, or open directory
                    // todo 
                    if (item.IsDir)
                    {
                        info.IsDirectory = true;
                        info.Context = new object();

                        // must set it to something if you return DokanError.Success
                        return DokanResult.Success;
                    }

                    break;

                case FileMode.CreateNew:
                    if (item != null)
                    {
                        return DokanResult.FileExists;
                    }
 
                    break;

                case FileMode.Truncate:
                    if (item == null)
                    {
                        return DokanResult.FileNotFound;
                    }

                    break;
            }

            if (readWriteAttributes)
            {
                info.Context = new object();
                return DokanResult.Success;
            }

            return await MainOpenFile(fileName, access, share, mode, options, info);
        }
  
        private async Task<NtStatus> MainOpenFile(string fileName, FileAccess access, FileShare share, FileMode mode, FileOptions options, DokanFileInfo info)
        {
            Console.WriteLine("VirtualDrive, MainCreateFile -> MainOpenFile: {0}, mode {1}", fileName, mode );

            var readAccess = (access & DataReadAccess) != 0;
            var writeAccess = (access & DataWriteAccess) != 0;

            if (writeAccess && ReadOnly)
            {
                return DokanResult.AccessDenied;
            }

            var ioaccess = System.IO.FileAccess.Read;
            if (!readAccess && writeAccess)
            {
                ioaccess = System.IO.FileAccess.Write;
            }

            if (readAccess && writeAccess)
            {
                ioaccess = System.IO.FileAccess.ReadWrite;
            }

            var result = await provider.OpenFile(fileName, mode, ioaccess, share, options);

            if (result == null)
            {
                return DokanResult.AccessDenied;
            }

            info.Context = result;
            return DokanResult.Success;
        }

        private NtStatus MainCreateDirectory(string fileName)
        {
            Log.Trace(GetType().Name, nameof(MainCreateDirectory), fileName);

            return Wait(CheckCreateDir(fileName));
        }

        private async Task<NtStatus> CheckCreateDir(string fileName)
        {
            Log.Trace(GetType().Name, nameof(CheckCreateDir), fileName);

            //if (string.IsNullOrEmpty(fileName))
            //    return DokanResult.Success;

            if (string.IsNullOrEmpty(fileName) | await provider.Exists(fileName))
            {
                return DokanResult.AlreadyExists;
            }

            await provider.CreateDir(fileName);
            return DokanResult.Success;
        }
        
        private bool HasAccess(DokanFileInfo info)
        {
            var identity = Processes.GetProcessOwner(info.ProcessId);
            if (identity == "NT AUTHORITY\\SYSTEM" || identity == creator)
            {
                return true;
            }

            Console.WriteLine($"User {identity} has no access to drive {MountPath}\r\nCreator User is {creator} - Identity User is {identity}");
            return false;
        }

        public void Cleanup(string fileName, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(Cleanup), fileName, info);

            try
            {
                if (info.DeleteOnClose)
                {
                    try
                    {
                        Wait(info.IsDirectory ? provider.DeleteDir(fileName) : provider.DeleteFile(fileName));
                    }
                    catch (AggregateException ex) when (ex.InnerException is FileNotFoundException)
                    {
                        Log.Error("File Not Found on Cleanup DeleteOnClose");
                    }
                }

            }
            catch (Exception e)
            {
               Console.WriteLine(e);
            }
        }

        public void CloseFile(string fileName, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(CloseFile), fileName, info);

            try
            {
                var stream = info.Context as IStream;
                if (stream != null)
                {
                    stream.Close();
                    stream.Dispose();
                    info.Context = null;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public NtStatus ReadFile(string fileName, byte[] buffer, out int bytesRead, long offset, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(ReadFile), fileName, info);

            var start = DateTime.UtcNow;
            try
            {
                var reader = info.Context as IStream;
                if (reader == null)
                {
                    throw new InvalidOperationException("reader is null");
                }

                bytesRead = Wait(reader.Read(offset, buffer, 0, buffer.Length, ReadTimeout - 1000));
                return DokanResult.Success;
            }
            catch (AggregateException ex) when (ex.InnerException is ObjectDisposedException)
            {
                bytesRead = 0;
                return NtStatus.FileClosed;
            }
            catch (AggregateException ex) when (ex.InnerException is NotSupportedException)
            {
                Log.Error("ReadWrite not supported: " + fileName);
                bytesRead = 0;
                return DokanResult.AccessDenied;
            }
            catch (AggregateException ex) when (ex.InnerException is TimeoutException)
            {
                Log.Error($"Timeout {(DateTime.UtcNow - start).TotalMilliseconds} File: {fileName}\r\n{ex.InnerException}");
                bytesRead = 0;
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error(e);
                bytesRead = 0;
                return DokanResult.Error;
            }
        }

        public NtStatus WriteFile(string fileName, byte[] buffer, out int bytesWritten, long offset, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(WriteFile), fileName, info);
        
            if (ReadOnly)
            {
                bytesWritten = 0;
                return DokanResult.AccessDenied;
            }

            var start = DateTime.UtcNow;
            try
            {
                var writer = info.Context as IStream;
                if (writer != null)
                {
                    Wait(writer.Write(offset, buffer, 0, buffer.Length));
                    bytesWritten = buffer.Length;
                    return DokanResult.Success;
                }

                bytesWritten = 0;
                return DokanResult.AccessDenied;
            }
            catch (AggregateException ex) when (ex.InnerException is NotSupportedException)
            {
                Log.Error("ReadWrite not supported: " + fileName);
                bytesWritten = 0;
                return DokanResult.AccessDenied;
            }
            catch (AggregateException ex) when (ex.InnerException is TimeoutException)
            {
                Log.Error($"Timeout {(DateTime.UtcNow - start).TotalMilliseconds} File: {fileName}");
                bytesWritten = 0;
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error(e);
                bytesWritten = 0;
                return DokanResult.AccessDenied;
            }
        }

        public NtStatus FlushFileBuffers(string fileName, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(FlushFileBuffers), fileName, info);

            try
            {
                (info.Context as Stream)?.Flush();

                return DokanResult.Success;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return DokanResult.Error;
            }
        }

        public NtStatus GetFileInformation(string fileName, out FileInformation fileInfo, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(GetFileInformation), fileName, info);

            if (!HasAccess(info))
            {
                fileInfo = default(FileInformation);
                return DokanResult.AccessDenied;
            }

            try
            {
                string streamName = null;
                fileName = fileName.Replace("\\:", ":");

                if (fileName.Contains(':'))
                {
                    var names = fileName.Split(':');
                    fileName = names[0];
                    streamName = names[1];
                }

                var nodeinfo = MakeFileInformation(fileName);
                if (nodeinfo != null)
                {
                    fileInfo = (FileInformation)nodeinfo;
                    if (streamName != null)
                    {
                        fileInfo.Length = 1;
                    }

                    return DokanResult.Success;
                }

                fileInfo = default(FileInformation);
                return DokanResult.PathNotFound;
            }
            catch (TimeoutException e)
            {
                Log.Error($"Timeout : {fileName}");
                fileInfo = default(FileInformation);
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error(e);
                fileInfo = default(FileInformation);
                return DokanResult.Error;
            }
        }

        public NtStatus FindFiles(string fileName, out IList<FileInformation> files, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(FindFiles), fileName, info);

            if (!HasAccess(info))
            {
                files = null;
                return DokanResult.AccessDenied;
            }

            try
            {
                var items = Wait(provider.GetDirItems(fileName), 10000);

                files = items.Select(i => new FileInformation
                {
                    Length = i.Length,
                    FileName = i.Name,
                    Attributes = i.IsDir ? FileAttributes.Directory : FileAttributes.Normal,
                    LastAccessTime = i.LastAccessTime,
                    LastWriteTime = i.LastWriteTime,
                    CreationTime = i.CreationTime
                }).ToList();

                return DokanResult.Success;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                files = new List<FileInformation>();
                return DokanResult.Error;
            }
        }

        public NtStatus FindFilesWithPattern(string fileName, string searchPattern, out IList<FileInformation> files, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(FindFilesWithPattern), fileName, info);

            files = new List<FileInformation>();
            return DokanResult.NotImplemented;
        }

        public NtStatus SetFileAttributes(string fileName, FileAttributes attributes, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(SetFileAttributes), fileName, info);

            return DokanResult.Error;
        }

        public NtStatus SetFileTime(string fileName, DateTime? creationTime, DateTime? lastAccessTime, DateTime? lastWriteTime,
            DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(SetFileTime), fileName, info);

            if (ReadOnly)
            {
                return DokanResult.AccessDenied;
            }

            return DokanResult.Error;
        }

        public NtStatus DeleteFile(string fileName, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(DeleteFile), fileName, info);

            if (ReadOnly)
            {
                return DokanResult.AccessDenied;
            }

            try
            {
                Wait(provider.DeleteFile(fileName));
                // todo 
                //var newfile = info.Context as NewFileBlockWriter;
                //newfile?.CancelUpload();

                return DokanResult.Success;
            }
            catch (AggregateException ex) when (ex.InnerException is FileNotFoundException)
            {
                Log.Error(ex);
                return DokanResult.PathNotFound;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return DokanResult.Error;
            }
        }

        public NtStatus DeleteDirectory(string fileName, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(DeleteDirectory), fileName, info);

            if (ReadOnly)
            {
                return DokanResult.AccessDenied;
            }

            try
            {
                Wait(provider.DeleteDir(fileName));
                return DokanResult.Success;
            }
            catch (AggregateException ex) when (ex.InnerException is FileNotFoundException)
            {
                Log.Error(ex);
                return DokanResult.PathNotFound;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return DokanResult.Error;
            }
        }

        public NtStatus MoveFile(string oldName, string newName, bool replace, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(MoveFile), $"{oldName} => {newName}", info);

            if (!HasAccess(info))
            {
                return DokanResult.AccessDenied;
            }

            if (ReadOnly)
            {
                return DokanResult.AccessDenied;
            }

            try
            {
                Wait(provider.MoveFile(oldName, newName, replace));
                              
                return DokanResult.Success;
            }
            catch (AggregateException ex) when (ex.InnerException is TimeoutException)
            {
                Log.Error(ex);
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return DokanResult.Error;
            }
        }

        public NtStatus SetEndOfFile(string fileName, long length, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(SetEndOfFile), fileName, info);

            try
            {
                if (ReadOnly)
                {
                    return DokanResult.AccessDenied;
                }

                var file = info.Context as IStream;
                Contract.Assert(file != null, "file != null");

                if (file == null)
                {
                    throw new InvalidOperationException("file is null");
                }

                file.SetLength(length);
                Log.Trace(GetType().Name, nameof(SetEndOfFile), $"{fileName} to {length}");

                return DokanResult.Success;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return DokanResult.Error;
            }
        }

        public NtStatus SetAllocationSize(string fileName, long length, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(SetAllocationSize), $"{fileName} to {length}");

            return DokanResult.Success;
        }

        public NtStatus LockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(LockFile), fileName, info);

            try
            {
                return DokanResult.Success;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return DokanResult.Error;
            }
        }

        public NtStatus UnlockFile(string fileName, long offset, long length, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(UnlockFile), fileName, info);

            return DokanResult.Success;
        }

        public NtStatus GetDiskFreeSpace(out long freeBytesAvailable, out long totalNumberOfBytes, out long totalNumberOfFreeBytes,
            DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(GetDiskFreeSpace), info);

            try
            {
                var total = Wait(provider.GetTotalSize());

                freeBytesAvailable = total - Wait(provider.GetTotalUsedSpace());
                totalNumberOfBytes = total;
                totalNumberOfFreeBytes = freeBytesAvailable;

                return DokanResult.Success;
            }
            catch (Exception e)
            {
                freeBytesAvailable = 0;
                totalNumberOfBytes = 0;
                totalNumberOfFreeBytes = 0;
                Console.WriteLine(e);
                return DokanResult.Error;
            }
        }

        public NtStatus GetVolumeInformation(out string volumeLabel, out FileSystemFeatures features, out string fileSystemName,
            DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(GetVolumeInformation), info);

            volumeLabel = provider.VolumeName;
            features =
                FileSystemFeatures.NamedStreams |
                FileSystemFeatures.SupportsRemoteStorage |
                FileSystemFeatures.CasePreservedNames |
                FileSystemFeatures.CaseSensitiveSearch |
                FileSystemFeatures.UnicodeOnDisk |
                //FileSystemFeatures.PersistentAcls|
                //FileSystemFeatures.SupportsEncryption |
                FileSystemFeatures.SequentialWriteOnce;

            if (ReadOnly)
            {
                features |= FileSystemFeatures.ReadOnlyVolume;
            }

            fileSystemName = provider.FileSystemName;
            return DokanResult.Success;
        }

        public NtStatus GetFileSecurity(string fileName, out FileSystemSecurity security, AccessControlSections sections,
            DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(GetFileSecurity),fileName, info);

            security = !info.IsDirectory
                ? (FileSystemSecurity)new FileSecurity()
                : new DirectorySecurity();
            return DokanResult.Success;
        }

        public NtStatus SetFileSecurity(string fileName, FileSystemSecurity security, AccessControlSections sections,
            DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(SetFileSecurity), fileName, info);

            return DokanResult.NotImplemented;
        }

        public NtStatus Mounted(DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(Mounted), info);

            return DokanResult.Success;
        }

        public NtStatus Unmounted(DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(Unmounted), info);

            return DokanResult.Success;
        }

        public NtStatus FindStreams(string fileName, out IList<FileInformation> streams, DokanFileInfo info)
        {
            Log.Trace(GetType().Name, nameof(FindStreams), fileName, info);

            streams = new List<FileInformation>();
            try
            {
                var item = provider.RetrieveItem(fileName).Result;
                if (item == null)
                {
                    return DokanResult.FileNotFound;
                }

                if (!item.IsDir)
                {
                    var infostream = MakeFileInformation(item);
                    streams.Add(infostream);
                }

                return DokanResult.Success;
            }
            catch (AggregateException ex) when (ex.InnerException is TimeoutException)
            {
                Log.Error(ex);
                return NtStatus.Timeout;
            }
            catch (Exception e)
            {
                Log.Error(e);
                return DokanResult.Error;
            }
        }

        private FileInformation? MakeFileInformation(string fileName)
        {
            Log.Trace(GetType().Name, nameof(MakeFileInformation), fileName);           

            var result = Wait(provider.GetItemInfo(fileName));
            if (result != null && ReadOnly)
            {
                var info = (FileInformation)result;
                info.Attributes |= FileAttributes.ReadOnly;
            }

            return result;
        }

        private FileInformation MakeFileInformation(FsItem i)
        {
            var result = new FileInformation
            {
                Length = i.Length,
                FileName = i.Name,
                Attributes = i.IsDir ? FileAttributes.Directory : FileAttributes.Normal,
                LastAccessTime = i.LastAccessTime,
                LastWriteTime = i.LastWriteTime,
                CreationTime = i.CreationTime
            };

            if (ReadOnly)
            {
                result.Attributes |= FileAttributes.ReadOnly;
            }

            return result;
        }
    
    }

    internal class DummyStream : AbstractStream
    {
        public override void Flush()
        {
        }

        public override void SetLength(long len)
        {
        }

        protected override void Dispose(bool disposing)
        {
        }

        public override Task<int> Read(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            return Task.FromResult(0);
        }

        public override Task Write(long position, byte[] buffer, int offset, int count, int timeout = 1000)
        {
            return Task.FromResult(0);
        }
    }
}
