﻿using System;
using DokanNet;

namespace VirtualDrive
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            MountMirror();
            Console.ReadLine();
        }

        private static void MountMirror()
        {
            try
            {
                var mirror = new Mirror();
                mirror.Mount("n:\\", DokanOptions.DebugMode, 1);

                Console.WriteLine(@"Success");
            }
            catch (DokanException ex)
            {
                Console.WriteLine(@"Error: " + ex.Message);
            }
        }
    }

}